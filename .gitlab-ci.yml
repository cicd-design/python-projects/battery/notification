include:
   # Python template
  - project: "to-be-continuous/python"
    ref: "6.4"
    file: "/templates/gitlab-ci-python.yml"   
  # GitLeak
  - project: 'to-be-continuous/gitleaks'
    ref: '2.1.1'
    file: '/templates/gitlab-ci-gitleaks.yml'
  # Docker template
  - project: 'to-be-continuous/docker'
    ref: '3.5.2'
    file: '/templates/gitlab-ci-docker.yml'
  # Semantic Release versionning and release management
  - project: 'to-be-continuous/semantic-release'
    ref: '3.2.2'
    file: '/templates/gitlab-ci-semrel.yml'


## Global variables
variables:
  # Docker Vars
  DOCKER_BUILD_ARGS: "--build-arg CI_PROJECT_URL --cache-ttl=6h" ## $CI_PROJECT_URL is used in the image manifest
  # New value for Snapshot image tag
  DOCKER_SNAPSHOT_IMAGE: $CI_REGISTRY_IMAGE/snapshot:$CI_COMMIT_SHORT_SHA
  ## Original value for Finale Release image tag
  DOCKER_RELEASE_IMAGE: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME

  # Kubernetes & Kustomize
  KUSTOMIZE_VER: "5.0.0"
  KUBECTL_VER: "1.25.4"
  K8S_KUBECTL_IMAGE: "registry.hub.docker.com/bitnami/kubectl:${KUBECTL_VER}-debian-11-r8" # client version matching OCI OKE 1.25.4
  K8S_SCORE_EXTRA_OPTS: "--kubernetes-version v${KUBECTL_VER} --ignore-test container-image-tag"
  K8S_URL: "https://***.***.***.*:***" # Kubernetes Cluster
  # K8S_CA_CERT & K8S_TOKEN must be defined as secret CI/CD variables
  K8S_BASE_APP_NAME: "cad" # override base application name
  # enable review, staging & prod (same namespace)
  K8S_REVIEW_SPACE: "dev-batterynotification"
  K8S_STAGING_SPACE: "test-batterynotification"
  K8S_PROD_SPACE: "prod-batterynotification"
  K8S_ENVIRONMENT_URL: "http://%{environment_name}.battery-notification.com"
  
  # Cypress & Postman: enable test on review environments
  REVIEW_ENABLED: "false"
  ROBOT_LINT_DISABLED: "true"

  # Helm Packaging
  HELM_BASE_APP_NAME: "batterynotification-ui"
  HELM_CHART_DIR: "./helm/batterynotification-ui"
  HELM_REVIEW_ENABLED: "false"

  # Semantic
  SEMREL_CHANGELOG_ENABLED: "true"

# Pipeline steps
stages:
  - build
  - test
  - package-build
  - package-test
  - review
  - staging
  - infra
  - deploy
  - acceptance
  - publish
  - infra-prod
  - production
  - gitops

gitleaks:
  stage: test
  script:
    - export GIT_AUTHOR_NAME="********"
    - export GIT_COMMITTER_NAME="********"
    - echo "Git username masked."
  
docker-sbom:
  artifacts:
    reports:
      cyclonedx:
        - reports/docker-sbom*.cyclonedx.json

gitlab-kustomize-gitops:
  stage: gitops
  image: line/kubectl-kustomize:latest
  before_script:
    - apk add git
    - CI_PROJECT_URL_SHORT=`echo "${CI_PROJECT_URL}" | sed -e "s/https\:\/\///"`
    - git remote set-url origin https://${SEMANTIC_USERNAME}:${SEMANTIC_PASSWORD}@${CI_PROJECT_URL_SHORT}.git
    - git config --global user.name "********"
    - git config --global user.email "********"
    - echo "Git author name and email masked."
    - NAMESPACE="dev-batternotification"
    - K8S_URL="https://***.***.***.*:***" # Kubernetes Cluster 
    - if [ "$CI_COMMIT_REF_NAME" == "development" ]; then NAMESPACE="dev-batterynotification"; fi
    - if [ "$CI_COMMIT_REF_NAME" == "test" ]; then NAMESPACE="test-batterynotification"; fi
    - if [ "$CI_COMMIT_REF_NAME" == "production" || "$CI_COMMIT_REF_NAME" == "main" || "$CI_COMMIT_REF_NAME" == "master" ]; then NAMESPACE="prod-batterynotification"; fi
  script:
    - git checkout -B $CI_COMMIT_REF_NAME 
    - cd k8s
    - kustomize edit set image $DOCKER_SNAPSHOT_IMAGE
    - kustomize edit set namespace $NAMESPACE
    - cat kustomization.yaml
    - git commit -am "[skip ci] $CI_COMMIT_REF_NAME image update $DOCKER_SNAPSHOT_IMAGE"
    - git push origin $CI_COMMIT_REF_NAME
  rules:
    - if: $CI_JOB_NAME == "docker-kaniko-build" && $CI_JOB_STATUS == "success"
      when: always
    - when: on_success
