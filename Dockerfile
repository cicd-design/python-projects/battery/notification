# Use a slim Python base image
FROM python:3.9-slim-buster
ARG CI_PROJECT_URL

COPY battery.py .

# Install the project dependencies
RUN pip install --upgrade pip

FROM nginx:alpine-slim
ARG CI_PROJECT_URL
RUN apk update --no-cache && apk upgrade --no-cache

COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf

COPY . /usr/share/nginx/html

RUN mkdir -p /var/cache/nginx/ && chown -R nginx:nginx /etc/nginx/ /var/cache/nginx /usr/share/nginx/
RUN ls -la /usr/share/nginx/html/


# Set the entry point for the container
CMD ["python", "battery.py"]
